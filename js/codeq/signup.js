/* CodeQ: an online programming tutor.
   Copyright (C) 2015 UL FRI

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>. */

/**
 * Created by robert on 10/14/15.
 *
 */

(function(){
    "use strict";
    var jqScreen = $("#screen-signup"),
        jqUsername = $("#signup-username"),
        jqName = $("#signup-name"),
        jqEmail = $("#signup-email"),
        jqPassword = $("#signup-password"),
        jqVerify = $("#signup-password-verify"),
        jqFormSignUp = $('#signup-form'),
        jqExitSignUpBtn = $('#signup-cancel'),
        jqLangSelect = jqScreen.find('.lang-select'),
        jqNavBarRight = $('.nav.navbar-nav.navbar-right'),
        jqNavigationHomeBtn = $('#navigation-home'),
        jqMessages = jqFormSignUp.find('.error,.success'),
        redirectTimeout = null;

    codeq.globalStateMachine.register('signup',{
        'jqScreen': jqScreen,

        'enter': function() {
            jqMessages.hide();
            jqNavigationHomeBtn.off('click');//remove the click listener of this element here
            jqNavBarRight.css('display','none');//hide settings etc.
            $('#signed-in-title').html('');
            jqScreen.css('display', '');
            $('#disabled').css('display', 'none');
            jqExitSignUpBtn.on('click',function(){
                codeq.globalStateMachine.transition('login');
            });

            //prepare listener for successfull signup
            jqFormSignUp.on('submit', function(e) {
                jqMessages.hide();
                if (jqPassword.val() != jqVerify.val()) {
                    jqFormSignUp.find('.error.mismatch').show();
                }
                else {
                    codeq.comms.signup(jqUsername.val(), jqName.val(), jqEmail.val(), jqPassword.val())
                        .then(function (data) {
                            var jqSuccess = jqFormSignUp.find('.success'),
                                username = jqUsername.val();
                            switch (data.code) {
                            case 0:  // everything OK
                                $("#username").val(username);
                                jqSuccess.find('.username').text(username);
                                jqSuccess.show();
                                // back to login after short pause
                                redirectTimeout = setTimeout(function() {
                                    codeq.globalStateMachine.transition('login');
                                }, 2000);
                                break;
                            case 10:  // username exists
                                jqFormSignUp.find('.error.username-exists').show();
                                break;
                            default:
                                throw new Error(data.message);
                            }
                        })
                        .fail(function (error) {
                            var message = 'Signup failed: ' + error.message;
                            codeq.log.error(message, error);
                            alert(message);
                        })
                        .done();
                }
                e.preventDefault(); // Prevent the form from submitting via the browser.
            });

            // setup language selection links
            jqLangSelect.on('click', function (e) {
                codeq.setLang($(this).data('lang'));
                e.preventDefault();
            });
        },
        'exit' : function(){
            if (redirectTimeout !== null) {
                clearTimeout(redirectTimeout);
                redirectTimeout = null;
            }
            jqMessages.hide();
            jqScreen.css('display', 'none');
            //re-enable the click listener
            jqNavigationHomeBtn.on('click', function(e){
                codeq.globalStateMachine.transition('language');
                e.preventDefault();
            });
            //disable listeners on stuff from this page only
            jqExitSignUpBtn.off('click');
            jqFormSignUp.off('submit');
            jqLangSelect.off('click');
            //show the menu buttons
            jqNavBarRight.css('display','');

            jqUsername.val('');
            jqName.val('');
            jqEmail.val('');
            jqPassword.val('');
            jqVerify.val('');
        }
    });

})();
