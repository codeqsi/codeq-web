/* CodeQ: an online programming tutor.
   Copyright (C) 2015 UL FRI

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>. */

/* Detect if we're on a phone, browser and platform type */
(function browserDetection() {
    "use strict";
    var platform = navigator.platform,
        userAgent = navigator.userAgent,
        runningOnPhone = false, // default value, will be changed if detected otherwise
        browserType = 'unknown',
        platformType = 'unknown';

    // Apple platforms
    if ((/iPod/.test(platform)) || (/iPhone/.test(platform))) {
        runningOnPhone = true;
        browserType = 'safari';
        platformType = 'iapple';
    }
    else if (/iPad/.test(platform)) {
        runningOnPhone = false;
        browserType = 'safari';
        platformType = 'iapple';
    }
    else {
        // general test for phones
        if (/[Mm]obile/.test(userAgent)) {
            runningOnPhone = true;
        }

        if ((/MSIE/.test(userAgent) && /Touch/.test(userAgent)) || (/IEMobile/.test(userAgent))) {
            platformType = 'ietouch';
        }

        if (/BlackBerry/.test(userAgent) || /BB10/.test(userAgent)) {
            platformType = 'blackberry';
        }

        // general browser tests
        if (/Presto/.test(userAgent)) {
            browserType = 'opera';
        }
        else if (/Android/.test(userAgent)) {
            // Android platform: only firefox, webkit, and chrome are possible
            platformType = 'android';
            if (/Chrome/.test(userAgent)) {
                browserType = 'chrome';
            }
            else if (/Firefox/.test(userAgent)) {
                browserType = 'firefox';
            }
            else if (/Safari/.test(userAgent)) {
                browserType = 'webkit';
            }
        }
        else {
            // desktop, or other mobile platforms
            if (/Chrome/.test(userAgent)) {
                browserType = 'chrome';
            }
            else if (/Firefox/.test(userAgent)) {
                browserType = 'firefox';
            }
            else if (/Safari/.test(userAgent)) {
                browserType = 'webkit';
            }
            else if (/MSIE/.test(userAgent)) {
                browserType = 'ie';
            }
        }
    }

    codeq.runningOnPhone = runningOnPhone;
    codeq.browserType = browserType;
    codeq.platformType = platformType;
})();
