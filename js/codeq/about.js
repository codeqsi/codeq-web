/* CodeQ: an online programming tutor.
   Copyright (C) 2015 UL FRI

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>. */



/**
 * Created by markop on 11/09/15.
 *
 */
(function(){
    "use strict";
    var jqScreen = $("#screen-about"),
        jqBtnGoBack = $("#about-back");

    codeq.globalStateMachine.register('about',{
        'jqScreen': jqScreen,
        'isModal': true,

        'enter': function(){
            jqBtnGoBack.on('click',function(){
                history.back();//forces a transition to the previous state
            });
            jqScreen.css('display', '');
        },
        'exit' : function(){
            jqScreen.css('display', 'none');
            jqBtnGoBack.off('click');
        }
    });

})();
