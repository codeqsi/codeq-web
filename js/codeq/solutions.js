/* CodeQ: an online programming tutor.
   Copyright (C) 2016 UL FRI

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>. */

(function(){
    "use strict";
    var jqScreen = $('#screen-solutions'),
        jqSolutions = jqScreen.find('.solutions'),
        jqBtnGoBack = jqScreen.find('.btnGoBack');

    // Register with the state machine
    codeq.globalStateMachine.register('solutions', {
        'jqScreen': jqScreen,
        'isModal': true,

        'enter': function (problem_ids) {
            jqBtnGoBack.on('click', function () {
                history.back();
            });
            codeq.comms.getSolutions(problem_ids)
            .then(function (solutions) {
                var i;
                solutions = solutions.data;
                for (i = 0; i < solutions.length; i++) {
                    jqSolutions.append('<pre>' + solutions[i].trim() + '</pre>');
                }
            })
            .done();
            jqScreen.css('display', '');
        },
        'exit' : function(){
            jqBtnGoBack.off('click');
            jqSolutions.empty();
            jqScreen.css('display', 'none');
        }
    });
})();
