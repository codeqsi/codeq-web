/* CodeQ: an online programming tutor.
   Copyright (C) 2015 UL FRI

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>. */

/**
 * Created by robert on 9/29/15.
 *
 */

(function(){
    "use strict";
    var navigationSettings = $('#navigation-settings'),
        guiLangSelect = $('#settings-gui-lang'),
        jqDisabledOverlay = $('#disabled'),
        jqSettForm = $("#settings-form"),
        jqSettCancelBtn = $("#settings-cancel"),
        jqScreenSettings = $('#screen-settings'),
        jqLayoutSelect = $('#settings-layout');

    codeq.on('layoutchange', function(){
        $("#screen-prolog").removeClass(codeq.supportedLayouts.join(" ")).addClass(codeq.getLayout());
        $("#screen-python").removeClass(codeq.supportedLayouts.join(" ")).addClass(codeq.getLayout());
    });

    codeq.globalStateMachine.register('settings',{
        'jqScreen': jqScreenSettings,
        'isModal': true,

        'enter':function(){
            jqDisabledOverlay.css('display', '');
            navigationSettings.addClass('active');

            var previousGuiLang = codeq.settings['gui_lang'];
            var previousLayout = codeq.settings['gui_layout'];
            jqSettForm.on("submit", function(e) {
                codeq.log.debug("settings for update:"+JSON.stringify(codeq.settings));
                codeq.comms.updateSettings(codeq.settings)
                    .then(function (data) {
                        if (data.code !== 0) {
                            throw new Error(data.message);
                        }
                        history.back();
                    })
                    .fail(function (error) {
                        var message = 'Updating settings failed: ' + error.message;
                        codeq.log.error(message, error);
                        alert(message);
                    })
                    .done();
                e.preventDefault();
            });
            jqSettCancelBtn.on("click",function(){
                jqLayoutSelect.val(previousLayout);
                if ($.inArray(previousLayout, codeq.supportedLayouts) >= 0) codeq.setLayout(previousLayout);
                guiLangSelect.val(previousGuiLang);
                if (previousGuiLang in codeq.supportedLangs) codeq.setLang(previousGuiLang);
                history.back();
            });
            guiLangSelect.on("change",function() {
                var lang = guiLangSelect.val();
                if (lang in codeq.supportedLangs) {
                    codeq.setLang(lang);
                }
            });
            jqLayoutSelect.on("change",function(){
                jqDisabledOverlay.css('display', '');
                var newLayout = jqLayoutSelect.val();
                if ($.inArray(newLayout, codeq.supportedLayouts) >= 0){
                    codeq.setLayout(newLayout);
                }
                jqDisabledOverlay.css('display', 'none');
            });
            jqScreenSettings.css('display', '');
            jqDisabledOverlay.css('display', 'none');
        },
        'exit':function(){
            jqSettForm.off('submit');
            jqSettCancelBtn.off('click');
            guiLangSelect.off('select');
            jqScreenSettings.css('display', 'none');
            navigationSettings.removeClass('active');
        }
    });
})();
