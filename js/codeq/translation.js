/* CodeQ: an online programming tutor.
   Copyright (C) 2015 UL FRI

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>. */

(function () {
    "use strict";
    var dicts = {},
        translateElement = function (jqElt, tkey, lang) {
            var dictionaryKey = jqElt.data('dict') || 'gui',
                translationKey = jqElt.data(tkey),
                dict = dicts[dictionaryKey],
                translations, html, key, att;
            if (dict === codeq.tr.emptyDictionary) return; // silent ignore
            if (!dict) {
                codeq.log.error('Cannot find translation dictionary ' + dictionaryKey);
                return;
            }
            if (!(typeof translationKey === 'number' || typeof translationKey === 'string')) {
                codeq.log.error('Cannot find the element\'s translation key, dictionary: ' + dictionaryKey);
                return;
            }
            translations = dict[translationKey];
            if (!translations) {
                codeq.log.error('Translation key ' + translationKey + ' is missing from dictionary ' + dictionaryKey);
                return;
            }
            html = translations[lang];
            if (!html) {
                html = translations['en'];
                if (html) {
                    codeq.log.info('There is no translation in language ' + lang + ' for key ' + translationKey + ' in dictionary ' + dictionaryKey + ', defaulting to language en');
                }
                else {
                    for (key in translations) {
                        if (!translations.hasOwnProperty(key)) continue;
                        html = translations[key];
                        if (!html) continue;
                        codeq.log.info('There is no translation in languages ' + lang + ' and en for key ' + translationKey + ' in dictionary ' + dictionaryKey + ', defaulting to language ' + key);
                        break;
                    }
                    if (!html) {
                        codeq.log.warn('There is no translation in any language for key ' + translationKey + ' in dictionary ' + dictionaryKey + ', leaving empty');
                        return;
                    }
                }
            }
            att = tkey.substring(5);
            if (att) jqElt.attr(att,html);
            else jqElt.html(html);
        },
        translateDocument = function (lang) {
            var keys = ['', 'placeholder', 'title', 'value', 'data-content'];
            keys.forEach(function (key) {
                var tkey = (key == '' ? 'tkey' : 'tkey-' + key),
                    selector = '[data-' + tkey + ']';
                $(selector).each(function () {
                    translateElement($(this), tkey, lang);
                });
            });
        };

    // Translate the whole document when the user switches the display language
    codeq.on('langchange', function (args) {
        translateDocument(args.lang);
    });

    // ================================================================================
    // The module API
    // ================================================================================

    codeq.tr = {
        'registerDictionary': function (name, dict) {
            dicts[name] = dict;
        },

        'unregisterDictionary': function (name) {
            delete dicts[name];
        },

        'emptyDictionary': {}, // use this with registerDictionary when you don't want any translations

        'translate': function (key, lang) {
            var dict = dicts['gui'];
            lang = lang || codeq.settings['gui_lang'];
            if (dict && dict[key] && dict[key][lang]) {
                return dict[key][lang];
            }
        },

        'translateDom': function (jqTopElt) {
            var lang = codeq.settings['gui_lang'];
            jqTopElt.filter('[data-tkey]').each(function () {
                translateElement(jqTopElt,'tkey', lang)
            });
            jqTopElt.find('[data-tkey]').each(function () {
                translateElement($(this),'tkey', lang);
            });
        }
    };
})();
