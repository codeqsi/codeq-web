/* CodeQ: an online programming tutor.
   Copyright (C) 2015,2016 UL FRI

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>. */

(function() {
    "use strict";
    var makeGlobalStateMachine = function (def) {
        var currState = null,
            waitingState = null; // state hidden with a modal form
        var stateChangeFun = function historyStateChangeHandler() {
            var historyState = History.getState();
            codeq.globalStateMachine.actualTransition.apply(
                codeq.globalStateMachine,
                $.merge([historyState.data.state],
                historyState.data.params));
        };

        History.Adapter.bind(window, 'statechange', stateChangeFun);
        // prepare the history state change listener
        return {
            'transition': function (name) {
                var newState = def[name];
                if (!newState) {
                    codeq.log.error('Cannot transition to undefined state: ' + name);
                    return;
                }
                if (newState === currState) {
                    codeq.log.info('Will not transition between identical states: ' + name);
                    return;
                }
                if (History.getState().data.state === name) {
                    // special case which happens if the user refreshes while
                    // in the login screen (history state doesn't change
                    // because it goes from login to login and the above
                    // listener doesn't trigger)
                    codeq.globalStateMachine.actualTransition.apply(
                        codeq.globalStateMachine,
                        Array.prototype.slice.apply(arguments, [0]));
                }
                try {
                    History.pushState(
                        {'state': name, 'params': Array.prototype.slice.apply(arguments, [1])},
                        'CodeQ', '?s=' + name);
                }
                catch (e) {
                    codeq.log.error('History.pushState() failed for new state ' + name + '. ' +
                                    'Error:' + e, e);
                }
            },
            'destroy': function () {
                if (currState) currState.exit();
                currState = null;
                if (waitingState) waitingState.exit();
                waitingState = null;
                History.Adapter.unbind(window, 'statechange', stateChangeFun);
            },
            'register': function (name, state, props) {
                if (name in def) {
                    codeq.log.error('State ' + name + ' is already registered, overriding.');
                }
                def[name] = state;
            },
            'actualTransition': function (name) {
                // always transition to 'login' state if no session exists
                // (except for 'signup' state)
                if (!codeq.comms.getSid() && name !== "signup") {
                    name = 'login';
                }

                // we have already checked that newState exists, and that it is
                // different from the current state
                var newState = def[name];
                if (currState) {
                    if (newState.isModal) {
                        if (currState.isModal) {
                            currState.exit();
                        }
                        else {
                            waitingState = currState;
                            waitingState.jqScreen.css('display', 'none');
                        }
                        currState = newState;
                        currState.enter.apply(currState, Array.prototype.slice.apply(arguments, [1]));
                    }
                    else {
                        currState.exit();
                        currState = newState;
                        if (waitingState === newState) {
                            waitingState.jqScreen.css('display', '');
                            waitingState = null;
                        }
                        else {
                            if (waitingState) {
                                waitingState.exit();
                                waitingState = null;
                            }
                            currState.enter.apply(currState, Array.prototype.slice.apply(arguments, [1]));
                        }
                    }
                }
                else {
                    currState = newState;
                    currState.enter.apply(currState, Array.prototype.slice.apply(arguments, [1]));
                }
            }
        }
    };
    codeq.globalStateMachine = makeGlobalStateMachine({});

    // setup the navigation buttons
    $('#navigation-home').on('click', function (e) {
        codeq.globalStateMachine.transition('language');
        e.preventDefault();
    });
    $('#navigation-problem-list').on('click', function (e) {
        codeq.globalStateMachine.transition('problem_list');
        e.preventDefault();
    });
    $('#navigation-python').on('click', function (e) {
        codeq.globalStateMachine.transition('python');
        e.preventDefault();
    });
    $('#navigation-prolog').on('click', function (e) {
        codeq.globalStateMachine.transition('prolog');
        e.preventDefault();
    });
    $('#navigation-logout').on('click', function (e) {
        codeq.comms.logout()
            .then(function (data) {
                codeq.comms.disconnect();
            })
            .fail(function (reason) {
                codeq.log.error('Logout failed: ' + reason);
            })
            .done();

        codeq.globalStateMachine.transition('login');
        e.preventDefault();
    });
    $('#navigation-profile').on('click', function (e) {
        codeq.globalStateMachine.transition('profile');
        e.preventDefault();
    });
    $('#navigation-about').on('click', function (e) {
        codeq.globalStateMachine.transition('about');
        e.preventDefault();
    });
    $('#navigation-change-password').on('click', function (e) {
        codeq.globalStateMachine.transition('changePassword');
        e.preventDefault();
    });
    $('#navigation-settings').on('click',function(e){
        codeq.globalStateMachine.transition('settings');
        e.preventDefault();
    });
})();
