/* CodeQ: an online programming tutor.
   Copyright (C) 2015 UL FRI

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>. */

/**
 * Created by robert on 9/17/15.
 *
 */

(function(){
    "use strict";
    var jqScreen = $("#screen-login"),
        jqNavBarRight = $('.nav.navbar-nav.navbar-right'),
        jqNavigationHomeBtn = $('#navigation-home'),
        jqForm = $('#login-form'),
        jqSignupBtn = $('#signup-button'),
        jqLangSelect = jqScreen.find('.lang-select'),
        jqDisabledOverlay = $('#disabled'),
        jqMessages = jqForm.find('.error');

    //the loginCallbackFunction is used here
    codeq.loginCallbackFunction = function (data) {
        if (data.code === 1) {
            throw new Error('Wrong username/password');
        }
        else if (data.code !== 0) {
            throw new Error('Login failed');
        }

        //nav signed in...
        $('#signed-in-title').html(data.name||data.username||data.email||'undefined');
        //merge with profile page
        $('#profile-username').html(data.username||data.email||'undefined');
        $('#profile-name').html(data.name||'undefined');
        $('#profile-email').html(data.email||'undefined');
        $('#profile-joined').html(new Date(data.joined).toLocaleString());
        $('#profile-last-login').html(new Date(data["last-login"]).toLocaleString());

        //merge these settings into the already existing default settings
        var sett = data.settings;
        $.extend(codeq.settings, sett);
        if('gui_lang' in sett && sett['gui_lang'] in codeq.supportedLangs){
            codeq.setLang(sett['gui_lang']);
            $("#settings-gui-lang").val(sett['gui_lang']);
        }
        if('gui_layout' in sett && ($.inArray(sett['gui_layout'], codeq.supportedLayouts) >= 0) ){
            codeq.setLayout(sett['gui_layout']);
            $("#settings-layout").val(sett['gui_layout']);
        }

        codeq.experiments = data.experiments || [];

        codeq.globalStateMachine.transition('language');
    };

    var loginFun = function() {
            jqMessages.hide();
            jqDisabledOverlay.show();
            codeq.comms.connect()
                .then(function () {
                    return codeq.comms.login($('#username').val(), $('#password').val());
                })
                .then(codeq.loginCallbackFunction)
                .fail(function (error) {
                    switch (error.message) {
                    case 'Wrong username/password':
                        jqForm.find('.error.wrong-user-or-pw').show();
                        break;
                    case 'Connection failed':
                        jqForm.find('.error.connection-failed').show();
                        break;
                    default:
                        jqForm.find('.error.login-failed').show();
                    }
                })
                .finally(function () {
                    jqDisabledOverlay.hide();
                })
                .done();
        };

    codeq.globalStateMachine.register('login',{
        'jqScreen': jqScreen,

        'enter': function(){
            jqNavigationHomeBtn.off('click');//remove the click listener of this element here
            jqNavBarRight.css('display','none');//hide settings etc.
            jqMessages.hide();
            $('#signed-in-title').html('');

            // setup login form
            jqForm.on('submit', function (e) {
                e.preventDefault();
                loginFun();
            });
            // setup the signup button
            jqSignupBtn.on('click', function(e){
                codeq.globalStateMachine.transition('signup');
                e.preventDefault();
            });
            // setup language selection links
            jqLangSelect.on('click', function (e) {
                codeq.setLang($(this).data('lang'));
                e.preventDefault();
            });

            jqScreen.css('display', '');
            jqDisabledOverlay.css('display', 'none');
        },
        'exit' : function(){
            //remove the listener from the buttons specific to this page
            jqForm.off('submit');
            jqSignupBtn.off('click');
            jqLangSelect.off('click');

            jqScreen.css('display', 'none');
            $("#password").val('');
            //re-enable the click listener
            jqNavigationHomeBtn.on('click', function(e){
                codeq.globalStateMachine.transition('language');
                e.preventDefault();
            });
            jqNavBarRight.css('display','');
            jqMessages.hide();
        }
    });
})();
