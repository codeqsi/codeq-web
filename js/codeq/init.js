/* CodeQ: an online programming tutor.
   Copyright (C) 2015,2016 UL FRI

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>. */

(function () {
    "use strict";
    var loadGuiTranslations = function () {
        var langs = codeq.availableLangs,
            loaders = [],
            loaderLangs = [],
            i, lang;
        for (i = langs.length - 1; i >= 0; i--) {
            lang = langs[i];
            loaders.push(codeq.comms.getGuiTranslation(lang));
            loaderLangs.push(lang);
        }
        return Q.all(loaders)
            .then(function (results) {
                // convert translations into something that the translation module can use
                var dictionary = {},
                    i, json, key, lang, translations;
                for (i = results.length - 1; i >= 0; i--) {
                    json = results[i];
                    lang = loaderLangs[i];
                    for (key in json) {
                        if (!json.hasOwnProperty(key)) continue;
                        translations = dictionary[key];
                        if (translations) translations[lang] = json[key];
                        else {
                            translations = {};
                            dictionary[key] = translations;
                            translations[lang] = json[key];
                        }
                    }
                }
                // ensure each key contains all translations
                for (key in dictionary) {
                    if (!dictionary.hasOwnProperty(key)) continue;
                    translations = dictionary[key];
                    for (i = langs.length - 1; i >= 0; i--) {
                        lang = langs[i];
                        if (!(lang in translations)) translations[lang] = "(Untranslated: " + lang + ")";
                    }
                }
                // register with the system
                codeq.tr.registerDictionary('gui', dictionary);
            });
    };

    // ================================================================================
    // The boot sequence
    // ================================================================================

    var Boot = function () {
        // set the language
        var navigatorLang = navigator.language || navigator.browserLanguage, // language reported by browser
            lang = null, // the translation language that will be chosen
            key;
        if (typeof navigatorLang === 'string') {
            navigatorLang = navigatorLang.split('-')[0]; // truncate the language variant, in eg. en-US
        }
        else navigatorLang = 'en';
        for (key in codeq.supportedLangs) {
            if (!codeq.supportedLangs.hasOwnProperty(key)) continue;
            if (key === navigatorLang) lang = key; // we support the browser's language
            codeq.availableLangs.push(key);
        }

        // the boot chain of async handlers: must be a sequence of .then() terminated with a .fail().done()
        loadGuiTranslations()
            .then(codeq.comms.getResourceTree)
            .then(function (resourceTree) {
                codeq.template.setResources(resourceTree); // save the loaded resource tree

                codeq.fire('init'); // tell any interested modules that we are now initialized, perhaps they want to initialize too
                codeq.fire('layoutchange'); // ensure a layout is set
                codeq.setLang(lang || 'en'); // initial language setting, this also translates the GUI
                // go to login
                codeq.globalStateMachine.transition('login');

                //For performance reasons, the Tooltip and Popover data-apis are opt-in, meaning you must initialize them yourself.
                $('[data-toggle="popover"]').popover()
            })
            .fail(function (error) {
                var message = 'CodeQ initialization failed: ' + error.message;
                codeq.log.error(message, error);
                alert(message);
            })
            .done();
    };

    if (codeq.isWebApp) {
        // we are a web app
        $(window).on('load', Boot);
        codeq.ajaxPrefix = location.pathname;
        if (location.protocol == 'https:') codeq.eioHost= 'wss://' + location.host;
        else codeq.eioHost= 'ws://' + location.host;
    }
    else {
        // we are a phonegap native app
        document.addEventListener("deviceready", Boot, false);
        // production
        //codeq.ajaxPrefix = 'http://codeq.si/';
        //codeq.eioHost= 'ws://codeq.si';
        // dev
        codeq.ajaxPrefix = 'http://212.235.189.51:22180/';
        codeq.eioHost= 'ws://212.235.189.51:22180';
    }

})();
