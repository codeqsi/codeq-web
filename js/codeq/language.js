/* CodeQ: an online programming tutor.
   Copyright (C) 2015 UL FRI

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>. */

/**
 * Created by robert on 9/18/15.
 *
 */

(function(){
    "use strict";
    var jqScreen = $('#screen-language'),
        jqProlog = $('#choose-prolog'),
        jqPython = $('#choose-python'),
        choose = function (language) {
            codeq.globalStateMachine.transition('problem_list', language);
        };

    codeq.globalStateMachine.register('language',{
        'jqScreen': jqScreen,

        'enter': function(){
            jqScreen.css('display', '');
            jqProlog.on('click', function (e) { e.preventDefault(); choose('prolog') });
            jqPython.on('click', function (e) { e.preventDefault(); choose('python') });
        },
        'exit' : function(){
            jqProlog.off();
            jqPython.off();
            jqScreen.css('display', 'none');
        }
    });
})();
