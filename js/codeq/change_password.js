/* CodeQ: an online programming tutor.
   Copyright (C) 2015,2016 UL FRI

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>. */

(function () {
    "use strict";
    var jqScreen = $('#screen-change-password'),
        jqChangePassForm = $('#change-password-form'),
        jqNew = $('#change-password-new'),
        jqVerify = $('#change-password-verify'),
        jqCancelBtn = $('#change-password-cancel'),
        jqMessages = jqChangePassForm.find('.error,.success'),
        redirectTimeout = null;

    codeq.globalStateMachine.register('changePassword', {
        'jqScreen': jqScreen,
        'isModal': true,

        'enter': function () {
            jqChangePassForm.find('button,input').prop('disabled', false);
            jqMessages.hide();
            jqCancelBtn.on('click',function(){
                history.back();//forces a transition to the previous state
            });
            jqChangePassForm.on('submit', function (e) {
                jqMessages.hide();
                if (jqNew.val() != jqVerify.val()) {
                    jqChangePassForm.find('.error.mismatch').show();
                }
                else {
                    codeq.comms.changePassword(jqNew.val())
                        .then(function (data) {
                            switch (data.code) {
                            case 0:
                                jqChangePassForm.find('button,input').prop('disabled', true);
                                jqChangePassForm.find('.success').show();
                                redirectTimeout = setTimeout(function () {
                                    history.back();
                                }, 2000);
                                break;
                            default:
                                jqChangePassForm.find('.error.failed').show();
                            }
                        })
                        .fail(function (error) {
                            var message = 'Changing password failed: ' + error.message;
                            codeq.log.error(message, error);
                            alert(message);
                        })
                        .done();
                }
                e.preventDefault(); // Prevent the form from submitting via the browser.
            });

            jqScreen.css('display', '');
            $('#disabled').css('display', 'none');
        },
        'exit': function () {
            if (redirectTimeout !== null) {
                clearTimeout(redirectTimeout);
                redirectTimeout = null;
            }
            jqChangePassForm.find('button,input').prop('disabled', false);
            jqMessages.hide();
            jqChangePassForm.off('submit');
            jqCancelBtn.off('click');
            jqScreen.css('display', 'none');
            jqNew.val('');
            jqVerify.val('');
        }
    });
})();
