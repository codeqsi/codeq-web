/* CodeQ: an online programming tutor.
   Copyright (C) 2015 UL FRI

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>. */

/**
 * Created by markop on 9/29/15.
 *
 */

(function(){
    "use strict";
    var jqScreen = $("#screen-profile"),
        jqBtnChangePass = $("#profile-change-password"),
        jqBtnGoBack = $("#profile-back");

    codeq.profile = {
    };

    codeq.globalStateMachine.register('profile',{
        'jqScreen': jqScreen,
        'isModal': true,

        'enter': function(){
            jqBtnChangePass.on('click',function(){
                codeq.globalStateMachine.transition('changePassword');
            });
            jqBtnGoBack.on('click',function(){
                history.back();//forces a transition to the previous state
            });
            jqScreen.css('display', '');
            $('#disabled').css('display', 'none');
            codeq.comms.getUserStat()
                .then(function (data) {
                    if (data.code !== 0) {
                        throw new Error(data.message);
                    }
                    data = data.stat;

                    var columns = ['language', 'problem_group', 'problems_count', 'done', 'in_progress'],
                        items='<thead><tr>';

                    $.each(columns, function( key, val ) {
                        items+='<th data-tkey="'+val+'">'+codeq.tr.translate(val)+'</th>';
                    });
                    items+='</tr></thead>';

                    $.each( data, function( object, row ) {
                        items+='<tr>';
                        if(row['problem_group']) {
                            $.each( columns, function( key, val ) {
                                items+='<td style="white-space: nowrap">'+row[val]||""+'</td>';
                            });
                        }
                        else {
                            $.each( columns, function( key, val ) {
                                items+='<td style="white-space: nowrap"><strong>'+(row[val]==null?'(all)':row[val])+'</strong></td>';
                            });
                        }
                        items+='</td>';
                    });

                    $('#profile-stats').html(items)

                })
                .fail(function (error) {
                    var message = 'Could not retrieve stats: ' + error.message;
                    codeq.log.error(message, error);
                    alert(message);
                })
                .done();
        },
        'exit' : function(){
            jqBtnChangePass.off('click');
            jqBtnGoBack.off('click');
            jqScreen.css('display', 'none');
        }
    });
})();
