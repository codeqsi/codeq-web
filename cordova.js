// differentiates between a native Phonegap/Cordova app, and a web app
// a Phonegap/Cordova build injects a native version of this file
codeq.isWebApp = true;